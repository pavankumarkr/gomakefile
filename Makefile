.PHONY: all test build env

all: test build env

test:
	go test -v ./src/jnwhiteh.net/buffers	
	
build:
	go build ./src/hello 

env:
	go env GOPROXY=https://proxy.golang.org,github.com/google/go-cmp,direct


